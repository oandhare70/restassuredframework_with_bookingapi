package Runner;

import java.io.IOException;

import CommonMethods.Utility;
import TestCases.Api_HealthCheck;
import TestCases.CreateAuth;
import TestCases.CreateBooking;
import TestCases.DeleteBooking;
import TestCases.UpdateBooking;
import TestCases.getBooking;

public class API_Runner {

	public static void main(String[] args) throws IOException {
		Api_HealthCheck.Executor();
		CreateAuth.Executor();
		CreateBooking.Executor();
		getBooking.Executor();
		UpdateBooking.Executor();
		getBooking.Executor();
		DeleteBooking.Executor();
		
//		Utility.ReadExcelData("CreateUser_Data", "TestCase_1");
	}
}
