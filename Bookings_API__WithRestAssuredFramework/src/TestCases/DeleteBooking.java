package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Repository.RequestBody;
import io.restassured.response.Response;

public class DeleteBooking {

	static String bookingid = CreateBooking.bookingId;

	public static void Executor() throws IOException {
		File folder = Utility.createLogDirectory("DeleteBooking_Log");
		String endpoint = RequestBody.Hostname() + RequestBody.resource_deleteBooking() + bookingid;
		int Statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.DeleteBooking(new String[] { "Content-Type", "Cookie" },
					new String[] { "application/json", "token=" + CreateAuth.token }, RequestBody.DeleteBooking(),
					endpoint);
			Statuscode = response.getStatusCode();
			if (Statuscode == 201) {
				Utility.CreateEvidenceFile(folder, Utility.createFileName("DeleteBooking"), endpoint,
						RequestBody.DeleteBooking(), response.getHeader("date"), response.getBody().asPrettyString(),
						bookingid);
				Validator(response);
				break;
			} else {
				System.out.println("Expected status code not found at" + i + "iteration hence retrying");
			}
		}
		if (Statuscode != 201) {
			System.out
					.println("Expected statuscode not found after 5 iterations hence not able to delete booking data");
			Assert.assertEquals(Statuscode, 201);
		}
	}

	public static void Validator(Response response) {
		Assert.assertEquals(response.statusCode(), 201);
		Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 201 Created");
		if (response.getStatusCode() == 201) {
			System.out.println("booking of ID:: " + CreateBooking.bookingId + " is deleted");
		} else {
			System.out.println("booking is not being able to delete");
		}

	}

}
