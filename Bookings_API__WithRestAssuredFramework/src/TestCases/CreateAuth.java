package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Repository.RequestBody;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class CreateAuth {

	public static String token;

	public static String Executor() throws IOException {

		File folder = Utility.createLogDirectory("CreateAuth_Log");
		String Endpoint = RequestBody.Hostname() + RequestBody.resource_Auth();
		int Statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.Auth(RequestBody.HeaderName(), RequestBody.HeaderValue(),
					RequestBody.AuthBody(), Endpoint);
//			System.out.println(response.getBody().asPrettyString());
			Statuscode = response.statusCode();
			if (Statuscode == 200) {
				Utility.CreateEvidenceFile(folder, Utility.createFileName("CreateAuth"), Endpoint, null,
						response.getHeader("date"), response.getBody().asPrettyString(), null);
				Validator(response);
				break;
			}
			else {
				System.out.println("expeted status code not found at"+i+"iteration hence retrying");
			}
		}
		if(Statuscode!=200) {
			System.out.println("expected status code and token is not generated even after 5 iterations");
			Assert.assertEquals(Statuscode,200);
		}

		
		return token;
	}
	public static void Validator(Response response) {
		ResponseBody resbody =response.getBody(); 
		token = resbody.jsonPath().getString("token");
		Assert.assertNotNull(token);
		
	}
}
