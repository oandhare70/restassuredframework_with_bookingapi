package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Repository.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class getBooking extends CreateBooking {

	public static void Executor() throws IOException {

		File folder = Utility.createLogDirectory("GetBooking_Log");
		String endpoint = RequestBody.Hostname() + RequestBody.resource_getBookings() + bookingId;
		int Statuscode = 0;
		for(int i=0;i<5;i++) {
			Response response = API_Trigger.getBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), endpoint);
			Statuscode = response.statusCode();
			if(Statuscode==200) {
				Utility.CreateEvidenceFile(folder, Utility.createFileName("GETBooking"), endpoint, "Request body is not given",
						response.getHeader("date"), response.getBody().asPrettyString(), bookingId);
				Validator(response);
				break;
			}
			else {
				System.out.println("Expected status code not found at "+i+" hence iterating again");
			}
		}
		
		if(Statuscode!=200) {
			System.out.println("expected statuse code code not found after 5 iterations hence not able to get resource");
		}

	}
	
	public static void Validator(Response response){
		ResponseBody resbody = response.getBody();
//		get response parameters
		
		Assert.assertNotNull(bookingId);
	}

}
