package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Repository.RequestBody;
import io.restassured.response.Response;

public class Api_HealthCheck {

	public static void Executor() throws IOException {
		File folder = Utility.createLogDirectory("checkAPI_Log");
		String Endpoint = RequestBody.Hostname() + RequestBody.resource_APICheck();
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.ApiCheck(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);
			statuscode = response.statusCode();
			if (statuscode == 201) {
				Utility.CreateEvidenceFile(folder, Utility.createFileName("CheckAPILog"), Endpoint,
						"request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
						null);
				validator(response);
				break;
			} else {
				System.out.println("Expected status code not found @" + i + "Iteration therfor retrying");
			}
		}
		if (statuscode != 201) {
			System.out.println("expected status code not found even after 5 iterations");
			Assert.assertEquals(statuscode, 201);
		}
	}

	public static void validator(Response response) {
		int statuscode = response.getStatusCode();
		Assert.assertEquals(statuscode, 201);
	}

}
