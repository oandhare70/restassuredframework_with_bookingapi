package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class UpdateBooking {

	
	static String bookingId = CreateBooking.bookingId;
	public static void Executor() throws IOException {
//		CreateAuth Authtoken = new CreateAuth();

		File folder = Utility.createLogDirectory("UpdateBooking_Log");
//		String bookingId = CreateBooking.bookingId;
		String Endpoint = RequestBody.Hostname() + RequestBody.resource_updateBooking() + bookingId;
		
		String reqbody = RequestBody.updateBooking("TestCase_5");
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.UpdateBooking(new String[] { "Content-Type", "Cookie" },
					new String[] { "application/json", "token=" + CreateAuth.token }, reqbody, Endpoint);
			statuscode = response.statusCode();
			if (statuscode == 200) {
				Utility.CreateEvidenceFile(folder, Utility.createFileName("UpdateBooking"), Endpoint,
						reqbody, response.getHeader("date"), response.getBody().asPrettyString(),
						bookingId);
				Validator(response, reqbody);
				break;
			} else {
				System.out.println("Expected status code not found after " + i + "iteration hence retrying ");
			}
		}
		if (statuscode != 200) {
			System.out.println(
					"expected status code not found even after 5 iterations hence booking data is not updated");
		}

	}

	public static void Validator(Response response, String reqbody) {
		
		ResponseBody resbody = response.getBody();
		
		String res_firstname = resbody.jsonPath().getString("firstname");
		String res_lastname = resbody.jsonPath().getString("lastname");
		String res_totalprice = resbody.jsonPath().getString("totalprice");
		String res_depositpaid = resbody.jsonPath().getString("depositpaid");
		String res_checkindate = resbody.jsonPath().getString("bookingdates.checkin");
		String res_checkoutdate = resbody.jsonPath().getString("bookingdates.checkout");

		String res_additionalneeds = resbody.jsonPath().getString("additionalneeds");

		// fetch request body parameters by using Jsonpath()
		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");
//		System.out.println(req_firstname);
		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");
		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates.checkin");
		String  req_checkoutdate = jpathreqbody.getString("bookingdates.checkout");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

		Assert.assertNotNull(bookingId);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		
		Assert.assertEquals(res_depositpaid, req_depositpaid);

		Assert.assertEquals(res_additionalneeds, req_additionalneeds);
	}
}