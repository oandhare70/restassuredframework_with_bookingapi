package Repository;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethods.Utility;

public class RequestBody extends Environment {
	
	public static String AuthBody() {
		String reqbody= "{\r\n"
				+ "    \"username\" : \"admin\",\r\n"
				+ "    \"password\" : \"password123\"\r\n"
				+ "}";
		return reqbody;
	}
	
	public static String CreateBooking(String Testcase) throws IOException {
		
		ArrayList<String> data = Utility.ReadExcelData("CreateUser_Data", Testcase);
		
		String key_firstname = data.get(1);
		String value_firstname = data.get(2);
		String key_lastname = data.get(3);
		String value_lastname = data.get(4);
		String key_totalprice = data.get(5);
		String value_totalprice = data.get(6);
		String key_depositpaid = data.get(7);
		String value_depositpaid = data.get(8);
		String key_checkin = data.get(9);
		String value_checkin = data.get(10);
		String key_checkout = data.get(11);
		String value_checkout = data.get(12);
		String key_additionalneeds = data.get(13);
		String value_additionalneeds = data.get(14);
		
		String reqbody = "{\r\n"
				+ "    \""+key_firstname+"\" : \""+value_firstname+"\",\r\n"
				+ "    \""+key_lastname+"\" : \""+value_lastname+"\",\r\n"
				+ "    \""+key_totalprice+"\" : "+value_totalprice+",\r\n"
				+ "    \""+key_depositpaid+"\" : "+value_depositpaid+",\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \""+key_checkin+"\" : \""+value_checkin+"\",\r\n"
				+ "        \""+key_checkout+"\" : \""+value_checkout+"\"\r\n"
				+ "    },\r\n"
				+ "    \""+key_additionalneeds+"\" : \""+value_additionalneeds+"\"\r\n"
				+ "}";
		return reqbody;
	}
	
	public static String updateBooking(String Testcase) throws IOException {
ArrayList<String> data = Utility.ReadExcelData("UpdateUser_Data", Testcase);
		
		String key_firstname = data.get(1);
		String value_firstname = data.get(2);
		String key_lastname = data.get(3);
		String value_lastname = data.get(4);
		String key_totalprice = data.get(5);
		String value_totalprice = data.get(6);
		String key_depositpaid = data.get(7);
		String value_depositpaid = data.get(8);
		String key_checkin = data.get(9);
		String value_checkin = data.get(10);
		String key_checkout = data.get(11);
		String value_checkout = data.get(12);
		String key_additionalneeds = data.get(13);
		String value_additionalneeds = data.get(14);
		String reqbody = "{\r\n"
				+ "    \""+key_firstname+"\" : \""+value_firstname+"\",\r\n"
				+ "    \""+key_lastname+"\" : \""+value_lastname+"\",\r\n"
				+ "    \""+key_totalprice+"\" : "+value_totalprice+",\r\n"
				+ "    \""+key_depositpaid+"\" : "+value_depositpaid+",\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \""+key_checkin+"\" : \""+value_checkin+"\",\r\n"
				+ "        \""+key_checkout+"\" : \""+value_checkout+"\"\r\n"
				+ "    },\r\n"
				+ "    \""+key_additionalneeds+"\" : \""+value_additionalneeds+"\"\r\n"
				+ "}";
		return reqbody;
	}
	public static String DeleteBooking() {
		String reqbody = "{\r\n"
				+ "    \"firstname\" : \"Onkar\",\r\n"
				+ "    \"lastname\" : \"Andhare\",\r\n"
				+ "    \"totalprice\" : 111,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2025-01-01\",\r\n"
				+ "        \"checkout\" : \"2029-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
				+ "}";
		return reqbody;
	}

}
